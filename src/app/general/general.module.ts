import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskOkDirective } from '../sevices/task-ok.directive';

@NgModule({
  declarations: [TaskOkDirective],
  imports: [
    CommonModule
  ],
  exports:[
    TaskOkDirective
  ]
})
export class GeneralModule { }
