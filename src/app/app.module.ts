import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { UkrformatPipe } from './sevices/ukrformat.pipe';
import { TaskOkDirective } from './sevices/task-ok.directive';
import { ProjectsModule } from './projects/projects.module';
import { TasksModule } from './tasks/tasks.module';
import { GeneralModule } from './general/general.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UkrformatPipe,
    
  ],
  imports: [
   
    BrowserModule,
    ProjectsModule,
    TasksModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
