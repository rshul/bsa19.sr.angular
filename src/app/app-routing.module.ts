import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from './sevices/can-deactivate.guard';
import { ProjectComponent } from './projects/project.component';
import { CreatProjectComponent } from './projects/creat-project/creat-project.component';
import { UpdateProjectComponent } from './projects/update-project/update-project.component';

const routes: Routes = [
  { path: '', redirectTo: '/projects', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
