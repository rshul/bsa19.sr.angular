import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[app-task-ok]'
})
export class TaskOkDirective {
  @Input('app-task-ok') isTaskOk: boolean;

  constructor(private el: ElementRef) {
    console.log(this.isTaskOk);
    if (this.isTaskOk) { console.log(this.isTaskOk) }
    const parent: HTMLElement = this.el.nativeElement.parentNode;
    const elem = document.createElement('span');
    elem.textContent = `⬤`;
    elem.style.color = 'green';
    elem.style.position = 'absolute';
    parent.insertBefore(elem, this.el.nativeElement);

  }
}
