import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'ukrformat'
})
export class UkrformatPipe implements PipeTransform {

  transform(value: string): string {
    let date = new Date(value);
    let tempDate = date.toJSON();
    tempDate = tempDate.split('T')[0];
    let [y,m,d]:string[] = tempDate.split('-');
    let ukrMonths = ["січня", "лютого", "березня", "квітня", "травня", "червня", "липня", "серпня", "вересня", "жовтня", "листопада", "грудня"];
    return `${d} ${ukrMonths[+m-1]} ${y}`;
  }

}
