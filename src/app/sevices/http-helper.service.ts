import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpHelperService {
  private baseUrl = 'https://localhost:5001/api';
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  getRequest<T>(url: string): Observable<T> {
    return this.http.get<T>(this.buildUrl(url));
  }

  postRequest<T>(url: string, body: any): Observable<T> {
    return this.http.post<T>(this.buildUrl(url), body, { headers: this.headers });
  }

  putRequest<T>(url: string, body: any): Observable<T> {
    return this.http.put<T>(this.buildUrl(url), body, { headers: this.headers });

  }

  public buildUrl(url: string): string {
    if (url.startsWith('http://') || url.startsWith('https://')) {
      return url;
    }
    return this.baseUrl + url;
  }
}
