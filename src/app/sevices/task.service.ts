import { Injectable } from '@angular/core';
import { TaskModel } from '../models/taskModel';
import { HttpHelperService } from './http-helper.service';
import { Observable } from 'rxjs';
import { NewTask } from '../models/newTask';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private taskRoute = '/Tasks';
  constructor(private request: HttpHelperService) { }

  getTasksAll(): Observable<TaskModel[]> {
    return this.request.getRequest<TaskModel[]>(this.taskRoute);
  }

  getTaskById(id: number): Observable<TaskModel> {
    return this.request.getRequest<TaskModel>(`${this.taskRoute}/${id}`);
  }

  updateTask(Task: TaskModel) {
    return this.request.putRequest<TaskModel>(this.taskRoute, Task);
  }

  createTask(Task: NewTask) {
    return this.request.postRequest<NewTask>(this.taskRoute, Task);
  }
}
