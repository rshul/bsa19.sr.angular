import { Project } from './../models/project';
import { HttpHelperService } from './http-helper.service';
import { Injectable } from '@angular/core';
import { NewProject } from '../models/new-project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectRoute = '/projects';
  constructor(private request: HttpHelperService) { }

  getProjectsAll(): Observable<Project[]> {
    return this.request.getRequest<Project[]>(this.projectRoute);
  }

  getProjectById(id: number): Observable<Project> {
    return this.request.getRequest<Project>(`${this.projectRoute}/${id}`);
  }

  updateProject(project: Project) {
    return this.request.putRequest<Project>(this.projectRoute, project);
  }

  createProject(project: NewProject) {
    return this.request.postRequest<NewProject>(this.projectRoute, project);
  }
}
