import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from 'src/app/sevices/dialog.service';
import { NewTask } from 'src/app/models/newTask';
import { Observable } from 'rxjs';
import { TaskService } from 'src/app/sevices/task.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.sass']
})
export class CreateTaskComponent implements OnInit {

  CreateTaskForm: FormGroup;
  isUpdated = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: TaskService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.CreateTaskForm = new FormGroup({
      name: new FormControl('', [Validators.required,
      Validators.minLength(4)]),
      description: new FormControl('', [Validators.required,
        Validators.minLength(4)]),
      created_At: new FormControl('',Validators.required),
      finished_At: new FormControl('',Validators.required),
      state: new FormControl('', Validators.required),
      project_Id: new FormControl('',Validators.required),
      performer_Id: new FormControl('', Validators.required)
    });

  }
  onSubmit() {
    const formValues: NewTask = this.CreateTaskForm.value;
    formValues.created_At = new Date(formValues.created_At).toISOString();
    formValues.finished_At = new Date(formValues.finished_At).toISOString();
    console.log(formValues);
    this.service.createTask(formValues).subscribe(_ => {this.isUpdated = true; this.router.navigate(['/tasks']);}, (er) => {
      return console.log(er);
    });
  }
  
  canDeactivate(): Observable<boolean> | boolean {
    if (this.isUpdated) {
      return true;
    }
    return this.dialogService.confirm('Discard changes?');
  }

}
