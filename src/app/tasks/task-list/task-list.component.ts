import { TaskModel } from './../../models/taskModel';
import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/sevices/task.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.sass']
})
export class TaskListComponent implements OnInit {

  public tasks$: Observable<TaskModel[]>;
  constructor(private taskService: TaskService) { }

  ngOnInit() {
   this.tasks$ = this.taskService.getTasksAll();
  }

}
