import { TaskModel } from './../../models/taskModel';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from 'src/app/sevices/project.service';
import { DialogService } from 'src/app/sevices/dialog.service';
import { TaskService } from 'src/app/sevices/task.service';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.sass']
})
export class UpdateTaskComponent implements OnInit {

  public projectId: number;
  CreateTaskForm: FormGroup;
  task$: Observable<TaskModel>;
  task: TaskModel;
  isUpdated = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: TaskService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.CreateTaskForm = new FormGroup({
      name: new FormControl('', [Validators.required,
      Validators.minLength(4)]),
      description: new FormControl('', [Validators.required,
        Validators.minLength(4)]),
      created_At: new FormControl('',Validators.required),
      finished_At: new FormControl('',Validators.required),
      state: new FormControl('', Validators.required),
      project_Id: new FormControl('',Validators.required),
      performer_Id: new FormControl('', Validators.required)
    });
    
    const id: number = +this.route.snapshot.paramMap.get('id');

    this.service.getTaskById(id).subscribe(x => {
      this.task = x; this.CreateTaskForm.setValue(
        {
          name: x.name,
          description: x.description,
          created_At: x.created_At.split('T')[0],
          finished_At: x.finished_At.split('T')[0],
          state: x.state,
          performer_Id: x.performer_Id,
          project_Id: x.project_Id
        });
    });
  }

  onSubmit() {

    const formValues: TaskModel = this.CreateTaskForm.value;
    formValues.created_At = new Date(formValues.created_At).toISOString();
    formValues.finished_At = new Date(formValues.finished_At).toISOString();
    const updatedTask = Object.assign(this.task, formValues);

    this.service.updateTask(updatedTask).subscribe( _ => {this.isUpdated = true; this.router.navigate(['/tasks']);}, (er) => {
      return console.log(er);
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.isUpdated) {
      return true;
    }
    return this.dialogService.confirm('Discard changes?');
  }

}
