import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskComponent } from './task/task.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { UpdateTaskComponent } from './update-task/update-task.component';
import { CanDeactivateGuard } from '../sevices/can-deactivate.guard';

const routes: Routes = [
  { path: 'tasks', component: TaskComponent },
  { path: 'tasks/create', component: CreateTaskComponent },
  { path: 'tasks/update/:id', component: UpdateTaskComponent, canDeactivate: [CanDeactivateGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
