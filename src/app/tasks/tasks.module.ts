import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TaskComponent } from './task/task.component';
import { UpdateTaskComponent } from './update-task/update-task.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { TaskListComponent } from './task-list/task-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskOkDirective } from '../sevices/task-ok.directive';
import { GeneralModule } from '../general/general.module';

@NgModule({
  declarations: [
    
    TaskComponent,
    UpdateTaskComponent,
    CreateTaskComponent,
    TaskListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TasksRoutingModule,
    GeneralModule
  ]
})
export class TasksModule { }
