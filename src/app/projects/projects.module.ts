import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { CreatProjectComponent } from './creat-project/creat-project.component';
import { ProjectsRoutingRoutingModule } from './projects-routing/projects-routing-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ProjectComponent,
    ProjectsListComponent,
    CreatProjectComponent,
    UpdateProjectComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ProjectsRoutingRoutingModule
  ]
})
export class ProjectsModule { }
