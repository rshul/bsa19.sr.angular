import { ProjectService } from './../../sevices/project.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.sass']
})
export class ProjectsListComponent implements OnInit {
  public projects$: Observable<Project[]>;
  constructor(private projectService: ProjectService) { }

  ngOnInit() {
   this.projects$ = this.projectService.getProjectsAll();
  }


}
