import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from '../project.component';
import { CreatProjectComponent } from '../creat-project/creat-project.component';
import { UpdateProjectComponent } from '../update-project/update-project.component';
import { CanDeactivateGuard } from 'src/app/sevices/can-deactivate.guard';

const routes: Routes = [
  { path: 'projects', component: ProjectComponent },
  { path: 'projects/create', component: CreatProjectComponent },
  { path: 'projects/update/:id', component: UpdateProjectComponent, canDeactivate: [CanDeactivateGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingRoutingModule { }
