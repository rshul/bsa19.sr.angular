import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from 'src/app/sevices/project.service';
import { DialogService } from 'src/app/sevices/dialog.service';
import { NewProject } from 'src/app/models/new-project';

@Component({
  selector: 'app-creat-project',
  templateUrl: './creat-project.component.html',
  styleUrls: ['./creat-project.component.sass']
})
export class CreatProjectComponent implements OnInit {
  CreateProjectForm: FormGroup;
  isUpdated = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ProjectService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.CreateProjectForm = new FormGroup({
      name: new FormControl('', [Validators.required,
      Validators.minLength(4)]),
      description: new FormControl('', [Validators.required,
        Validators.minLength(4)]),
      created_At: new FormControl('',Validators.required),
      deadline: new FormControl('',Validators.required),
      author_Id: new FormControl(''),
      team_Id: new FormControl('', Validators.required)
    });

  }
  onSubmit() {
    const formValues: NewProject = this.CreateProjectForm.value;
    formValues.created_At = new Date(formValues.created_At).toISOString();
    formValues.deadline = new Date(formValues.deadline).toISOString();
    console.log(formValues);
    this.service.createProject(formValues).subscribe(_ => {this.isUpdated = true; this.router.navigate(['/projects']);}, (er) => {
      return console.log(er);
    });
  }
  
  canDeactivate(): Observable<boolean> | boolean {
    if (this.isUpdated) {
      return true;
    }
    return this.dialogService.confirm('Discard changes?');
  }

}
