import { DialogService } from './../../sevices/dialog.service';
import { ProjectService } from './../../sevices/project.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project } from 'src/app/models/project';


@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.sass']
})
export class UpdateProjectComponent implements OnInit {
  public projectId: number;
  CreateProjectForm: FormGroup;
  project$: Observable<Project>;
  project: Project;
  isUpdated = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ProjectService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.CreateProjectForm = new FormGroup({
      name: new FormControl('',[Validators.required,
        Validators.minLength(4)]),
      description: new FormControl('',[Validators.required,
        Validators.minLength(4)]),
      created_At: new FormControl('', Validators.required),
      deadline: new FormControl('', Validators.required),
      author_Id: new FormControl(''),
      team_Id: new FormControl('', Validators.required)
    });
    // this.project$ = this.route.paramMap.pipe(
    //   switchMap((params: ParamMap) =>
    //     this.service.getProjectById(+params.get('id'))
    //   )
    // );
    // this.project$.subscribe(x => {

    // }
    //   , (error) => console.log(error));

    const id: number = +this.route.snapshot.paramMap.get('id');

    this.service.getProjectById(id).subscribe(x => {
      this.project = x; this.CreateProjectForm.setValue(
        {
          name: x.name,
          description: x.description,
          created_At: x.created_At.split('T')[0],
          deadline: x.deadline.split('T')[0],
          author_Id: x.author_Id,
          team_Id: x.team_Id
        });
    });
  }

  onSubmit() {

    const formValues: Project = this.CreateProjectForm.value;
    formValues.created_At = new Date(formValues.created_At).toISOString();
    formValues.deadline = new Date(formValues.deadline).toISOString();
    const updatedProject = Object.assign(this.project, formValues);

    this.service.updateProject(updatedProject).subscribe( _ => {this.isUpdated = true; this.router.navigate(['/projects']);}, (er) => {
      return console.log(er);
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.isUpdated) {
      return true;
    }
    return this.dialogService.confirm('Discard changes?');
  }

}
