export interface Project {
    id: number;
    name: string;
    description: string;
    created_At: string;
    deadline: string;
    author_Id: number;
    team_Id: number;
}
