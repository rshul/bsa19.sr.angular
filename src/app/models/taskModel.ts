export interface TaskModel {
    id: number;
    name: string;
    description: string;
    created_At: string;
    finished_At: string;
    state: string;
    project_Id: string;
    performer_Id: string;
}
